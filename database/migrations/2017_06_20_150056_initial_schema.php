<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('incomes', function (Blueprint $table) {
            $table->increments('inid');
            $table->integer('uid');
            $table->string('source');
            $table->decimal('amount');
            $table->timestamps();
        });

        Schema::create('expenses', function (Blueprint $table){
            $table->increments('exid');
            $table->integer('uid');
            $table->string('for');
            $table->decimal('amount');
            $table->timestamps();
        });
        //


        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('expense');
        Schema::drop('income');
    }
}
